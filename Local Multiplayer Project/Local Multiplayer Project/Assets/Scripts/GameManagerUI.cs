﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerUI : MonoBehaviour
{
    public string player1Join;
    public string player2Join;
    public string player3Join;
    public string player4Join;
    public Text player1Text;
    public Text player2Text;
    public Text player3Text;
    public Text player4Text;

    public GameManager gameManager;
    static public GameManagerUI instance;


    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        player1Text.text = "Press to Join!";
        player2Text.text = "Press to Join!";
        player3Text.text = "Press to Join!";
        player4Text.text = "Press to Join!";

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore(int newScore, int playerNumber)
    {
        switch (playerNumber)
        {
            case 1:
                player1Text.text = "Score: " + newScore;
                break;
            case 2:
                player2Text.text = "Score: " + newScore;
                break;
            case 3:
                player3Text.text = "Score: " + newScore;
                break;
            case 4:
                player4Text.text = "Score: " + newScore;
                break;

        }
    }
}
