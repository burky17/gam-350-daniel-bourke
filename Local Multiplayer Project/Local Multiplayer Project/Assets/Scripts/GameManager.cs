﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [SerializeField] private bool gameRunning = true;
    [SerializeField] private int numPlayers = 0;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] public bool player1Active = false;
    [SerializeField] public bool player2Active = false;
    [SerializeField] public bool player3Active = false;
    [SerializeField] public bool player4Active = false;






    public List<GameObject> playerSpawnpoints;
    public List<GameObject> foodSpawnpoints;
    public List<GameObject> allPlayers;

    static public GameManager instance;
    public GameManagerUI gameManagerUI;




    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        //foreach (GameObject player in allPlayers)
        //{
            
        //}
    }

    void Update()
    {
            if (Input.GetButtonDown("Submit1") && !player1Active)
            {
               GameObject Player1 = Instantiate(playerPrefab, playerSpawnpoints[0].transform.position, playerSpawnpoints[0].transform.rotation);            
                allPlayers.Add(Player1);
                Player1.GetComponent<PlayerController>().gameManager = this;
                Player1.GetComponent<PlayerController>().gameManagerUI = gameManagerUI;
                Player1.GetComponent<PlayerController>().playerNumber = 1;
                GameManagerUI.instance.UpdateScore(0, 1);
                player1Active = true;
                numPlayers++;
            }
            if (Input.GetButtonDown("Submit2") && !player2Active)
            {
                GameObject Player2 = Instantiate(playerPrefab, playerSpawnpoints[1].transform.position, playerSpawnpoints[1].transform.rotation);
                allPlayers.Add(Player2);
                Player2.GetComponent<PlayerController>().gameManager = this;
                Player2.GetComponent<PlayerController>().playerNumber = 2;
                GameManagerUI.instance.UpdateScore(0, 2);
                player2Active = true;
                numPlayers++;
            }
            if (Input.GetButtonDown("Submit3") && !player3Active)
            {
                GameObject Player3 = Instantiate(playerPrefab, playerSpawnpoints[2].transform.position, playerSpawnpoints[2].transform.rotation);
                allPlayers.Add(Player3);
                Player3.GetComponent<PlayerController>().gameManager = this;
                Player3.GetComponent<PlayerController>().playerNumber = 3;
                GameManagerUI.instance.UpdateScore(0, 3);
                player3Active = true;
                numPlayers++;
            }
            if (Input.GetButtonDown("Submit4") && !player4Active)
            {
                GameObject Player4 = Instantiate(playerPrefab, playerSpawnpoints[3].transform.position, playerSpawnpoints[3].transform.rotation);
                allPlayers.Add(Player4);
                Player4.GetComponent<PlayerController>().gameManager = this;
                Player4.GetComponent<PlayerController>().playerNumber = 4;
                GameManagerUI.instance.UpdateScore(0, 4);
                player4Active = true;
                numPlayers++;
            }
        
    }
}
