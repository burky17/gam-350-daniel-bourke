﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Rigidbody playerRigidbody;
    [SerializeField] private GameObject playerObject;
    [SerializeField] private Material playerMaterial;
    [SerializeField] private bool isJumping = false;
    [SerializeField] private bool isMidair = false;
    [SerializeField] private float horizontalAxis;
    [SerializeField] private float playerSpeed = 10;
    [SerializeField] private int playerJump = 400;
    [SerializeField] private int playerScore = 0;
    [SerializeField] public int playerNumber;    
    [SerializeField] private string controllerAxis;
    [SerializeField] private string controllerButton;
    [SerializeField] public string playerName;
    [SerializeField] private Text playerText;
    public GameManager gameManager;
    public GameManagerUI gameManagerUI;





    void Start()
    {        
        playerMaterial = GetComponent<Renderer>().material;

        switch (playerNumber)
        {
            case 1:
                controllerAxis = "Horizontal1";
                controllerButton = "Submit1";                
                playerMaterial.color = Color.magenta;
                playerName = "Player 1";
                break;
            case 2:
                controllerAxis = "Horizontal2";
                controllerButton = "Submit2";
                playerMaterial.color = Color.cyan;
                playerName = "Player 2";
                break;
            case 3:
                controllerAxis = "Horizontal3";
                controllerButton = "Submit3";
                playerMaterial.color = Color.yellow;
                playerName = "Player 3";
                break;
            case 4:
                controllerAxis = "Horizontal4";
                controllerButton = "Submit4";
                playerMaterial.color = Color.green;
                playerName = "Player 4";
                break;

        }

        playerText.text = playerName;

    }

    void FixedUpdate()
    {      

        horizontalAxis = Input.GetAxis(controllerAxis);         
             
        

        playerRigidbody.velocity = new Vector3(horizontalAxis * playerSpeed, playerRigidbody.velocity.y, 0);

        if (isJumping)
        {
            playerRigidbody.AddForce(0, playerJump, 0);
            isJumping = false;
        }
    }

    void Update()
    {
        if (Input.GetButtonDown(controllerButton) && !isMidair)
        {
            isJumping = true;
            isMidair = true;
        }
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        

        if (collision.transform.tag == "Floor")
        {           
            isMidair = false;
           
        }
        
    }

    private void OnTriggerEnter(Collider trigger)
    {
        if (trigger.transform.tag == "Food")
        {
            playerScore++;
            GameManagerUI.instance.UpdateScore(playerScore, playerNumber);
            Destroy(trigger.gameObject);
        }
    }
}
