﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour
{
    public Text m_healthText;
    public Text m_speedText;
    public Text m_jumpText;


    void Start()
    {
        GetComponent<StatSystem>().AddCallback(StatSystem.StatType.Health,
            HealthUpdate);

        FindObjectOfType<StatSystem>().AddCallback(StatSystem.StatType.Speed,
            SpeedUpdate);

        FindObjectOfType<StatSystem>().AddCallback(StatSystem.StatType.Jump,
            JumpUpdate);

        FindObjectOfType<StatSystem>().AddCallback(StatSystem.StatType.Power,
            PowerUpdate);
    }

    public void HealthUpdate(StatSystem.StatType aStat, int old, int newVal)
    {
        //GetComponent<UnityEngine.UI.Text>().text = newVal.ToString();
        m_healthText.text = "Health: " + newVal;
    }

    public void SpeedUpdate(StatSystem.StatType aStat, int old, int newVal)
    {
        //GetComponent<UnityEngine.UI.Text>().text = newVal.ToString();
        m_speedText.text = "Speed: " + newVal;
    }

    public void JumpUpdate(StatSystem.StatType aStat, int old, int newVal)
    {
        //GetComponent<UnityEngine.UI.Text>().text = newVal.ToString();
        m_jumpText.text = "Jump: " + newVal;
    }

    public void PowerUpdate(StatSystem.StatType aStat, int old, int newVal)
    {
        //GetComponent<UnityEngine.UI.Text>().text = newVal.ToString();
        m_jumpText.text = "Power: " + newVal;
    }


}
