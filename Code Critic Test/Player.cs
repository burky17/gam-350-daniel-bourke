﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Character controller
public class Player : MonoBehaviour
{
    public Rigidbody playerRigidbody;
    public GameObject playerObject;
    public GameObject skillTreeDisplay;
    public int jumps = 2;
    public bool skillTreeOn = false;
    public bool isJumping = false;
    public bool isMidair = false;
    public bool isSlamming = false;
    public bool startSlamming = false;
    public bool isAttacking = false;
    public bool isDefending = false;
    float horizontalAxis = 0;
    public int oldSpeed;
    public Text healthText;
    public Text speedText;
    public Text jumpText;
    public Text powerText;
    public StatSystem MyStats;
    

    [SerializeField]
    float currentSpeed;

    [SerializeField]
    float currentFriction;

    //Finds the StatSystem attached to the Player   
    //Initial update for displaying all Stats
    void Start()
    {        
        Weather.instance.SetPlayer(this);
        SkillTree.instance.SetPlayer(this);
        MyStats = GetComponent<StatSystem>();        
        MyStats.AddCallback(StatSystem.StatType.Health, HealthUpdate);
        MyStats.AddCallback(StatSystem.StatType.Speed, SpeedUpdate);
        MyStats.AddCallback(StatSystem.StatType.Jump, JumpUpdate);
        MyStats.AddCallback(StatSystem.StatType.Power, PowerUpdate);

        healthText.text = "Health: " + MyStats.GetValue(StatSystem.StatType.Health);
        speedText.text = "Speed: " + MyStats.GetValue(StatSystem.StatType.Speed);
        jumpText.text = "Jump: " + MyStats.GetValue(StatSystem.StatType.Jump);
        powerText.text = "Power: " + MyStats.GetValue(StatSystem.StatType.Power);

    }
    

    //Provides all character controller input
    //Player can Jump, Slam, and Crouch
    void FixedUpdate()
    {
        horizontalAxis = Input.GetAxis("Horizontal");

        if (isMidair)
        {
            playerRigidbody.AddForce(0, -Time.deltaTime, 0);
        }

        // Getting Axis for Horizontal Keyboard Input
        if (playerRigidbody.velocity.magnitude < 10)
        {
            if (isMidair)
            {
                playerRigidbody.AddForce(((MyStats.GetValue(StatSystem.StatType.Speed) / 4)) * horizontalAxis, playerRigidbody.velocity.y, 0);
            }
            else
            {
                playerRigidbody.AddForce((MyStats.GetValue(StatSystem.StatType.Speed)) * horizontalAxis * 2, playerRigidbody.velocity.y, 0);

            }
        }

        if (isJumping)
        {
            playerRigidbody.AddForce(0, MyStats.GetValue(StatSystem.StatType.Jump), 0);
            MyStats.UpdateStat(StatSystem.StatType.Power, 1);
            jumps--;
            isJumping = false;
        }

        if (startSlamming)
        {
            playerRigidbody.AddForce(0, (MyStats.GetValue(StatSystem.StatType.Jump) * -4), 0);
            startSlamming = false;
            isSlamming = true;
        }
    }

    // Update is called once per frame
    void Update()
    {         
        //Causes the player to jump when space is pressed, if they have sufficient jumps left.
        //Player can double jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (jumps > 0)
            {
                isJumping = true;
                isMidair = true;
            }
        }

        //Crouching with S, halves the player's vertical size and quarters player speed
        if (Input.GetKeyDown(KeyCode.S) && !isMidair)
        {
            transform.localScale = new Vector3(1, 0.5f, 1);
            oldSpeed = MyStats.GetValue(StatSystem.StatType.Speed);
            MyStats.SetStat(StatSystem.StatType.Speed, (oldSpeed / 2));
            playerRigidbody.AddForce((MyStats.GetValue(StatSystem.StatType.Speed)) * horizontalAxis,0, 0);
        }

        if (Input.GetKeyDown(KeyCode.S) && isMidair)
        {
            startSlamming = true;
        }

        if (Input.GetKeyDown(KeyCode.E) && !isAttacking)
        {
            SkillTree.instance.AttackAbility(MyStats.GetValue(StatSystem.StatType.Power));
        }

        if (Input.GetKeyDown(KeyCode.Q) && !isDefending)
        {
            SkillTree.instance.DefenseAbility(MyStats.GetValue(StatSystem.StatType.Power));
        }

        //Releasing S will reset the player scale and movement speed
        if (Input.GetKeyUp(KeyCode.S) && !isSlamming && !isMidair)
        {
            transform.localScale = new Vector3(1, 1, 1);
            if (oldSpeed > 0)
            {
                MyStats.SetStat(StatSystem.StatType.Speed, oldSpeed);
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!skillTreeOn)
            {
                skillTreeDisplay.SetActive(true);
                skillTreeOn = true;                
            }

            else if (skillTreeOn)
            {
                skillTreeDisplay.SetActive(false);
                skillTreeOn = false;                
            }
        }

        if (MyStats.GetValue(StatSystem.StatType.Health) <= 0)
        {
            Destroy(this);
        }

        currentSpeed = playerRigidbody.velocity.magnitude;
        currentFriction = gameObject.GetComponent<BoxCollider>().material.dynamicFriction;
    }
   
    //UI Updates for callbacks
    public void HealthUpdate(StatSystem.StatType aStat, int old, int newVal)
    {        
        healthText.text = "Health: " + newVal;
    }

    public void SpeedUpdate(StatSystem.StatType aStat, int old, int newVal)
    {        
        speedText.text = "Speed: " + newVal;
    }

    public void JumpUpdate(StatSystem.StatType aStat, int old, int newVal)
    {        
        jumpText.text = "Jump: " + newVal;
    }

    public void PowerUpdate(StatSystem.StatType aStat, int old, int newVal)
    {        
        powerText.text = "Power: " + newVal;
    }

    //Handle all collision interactions
    private void OnCollisionEnter(Collision collision)
    {
        //Resets double jump counter when landing on the floor

        if (collision.transform.tag == "Floor")
        {
            MyStats.UpdateStat(StatSystem.StatType.Power, ((2 - jumps) * -1));
            jumps = 2;    
            isMidair = false;
            isSlamming = false;
            isAttacking = false;
            isDefending = false;
        }
        
        //Triggers a "wall kick" when touching a "wall" tagged object, allowing
        // rapid back-and forth movement to climb up walls

        if (collision.transform.tag == "Wall")
        {
            playerRigidbody.velocity = new Vector3(0, 4, 0);            
        }         

    } 

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "SkillPoint")
        {
            SkillTree.instance.UpdateSkillPoints(1);
            Destroy(other.gameObject);
        }
    }
    
    //Below are helper functions used as middle-men to pass information to and from the 
    //private Player StatSystem

    //Allows other things to see the Player's Power Stat.currentValue
    public int ReturnPowerLevel()
    {
        return MyStats.GetValue(StatSystem.StatType.Power);
    }

    //Will adjust Player Health only
    public void TakeDamage(int damageVal)
    {
        MyStats.UpdateStat(StatSystem.StatType.Health, -damageVal);
    }

    //Used for Equipping items, will add applicable item stat values to the player stats
    public void AddStats(ItemDefinition a_item)
    {
        foreach (ItemDefinition.Stat a_Stat in a_item.itemStats)
        {
            if (a_Stat.statType == ItemDefinition.StatType.Health)
            {
                MyStats.UpdateStat(StatSystem.StatType.Health, a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Speed)
            {
                MyStats.UpdateStat(StatSystem.StatType.Speed, a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Jump)
            {
                MyStats.UpdateStat(StatSystem.StatType.Jump, a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Power)
            {
                MyStats.UpdateStat(StatSystem.StatType.Power, a_Stat.currentValue);
            }
        }
    }

    //Used for Unequipping items, will subtract applicable item stat values to the player stats

    public void RemoveStats(ItemDefinition a_item)     
    {
        foreach (ItemDefinition.Stat a_Stat in a_item.itemStats)
        {
            if (a_Stat.statType == ItemDefinition.StatType.Health)
            {
                MyStats.UpdateStat(StatSystem.StatType.Health, -a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Speed)
            {
                MyStats.UpdateStat(StatSystem.StatType.Speed, -a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Jump)
            {
                MyStats.UpdateStat(StatSystem.StatType.Jump, -a_Stat.currentValue);
            }

            if (a_Stat.statType == ItemDefinition.StatType.Power)
            {
                MyStats.UpdateStat(StatSystem.StatType.Power, -a_Stat.currentValue);
            }
        }
    }

    public void WeatherFriction(int frictionVal)
    {
        gameObject.GetComponent<BoxCollider>().material.dynamicFriction = frictionVal;
        gameObject.GetComponent<BoxCollider>().material.staticFriction = frictionVal;
    }
}
