﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TacticsClient : MonoBehaviour
{
    public ClientNetwork clientNet;

    public enum PlayerClass
    {
        Warrior = 1,
        Rogue = 2,
        Wizard = 3
    }


    public long clientId; // Unique client id
    public bool isConnected; // Is this client connected, false if they have been removed from the game

    public int thisPlayerID; // Assigned player id
    public int teamId; // Team id

    // Data that the client is allowed to set
    public string playerName;
    public bool isReady;
    public PlayerClass playerClass; 

    // Position on the board
    public int maxMoves;
    public int movesRemaining;
    public int xPosition;
    public int yPosition;
    public int mapX;
    public int mapY;
    public Vector2 currentPosition;
    public List<Vector2> blockedSpaces = new List<Vector2>();

    // Attack data
    public int attackRange;

    // Health
    public int maxHealth;
    public int health;

    // Get the instance of the client
    static TacticsClient instance = null;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;

    public GameObject myPlayer;

    // Singleton support
    public static TacticsClient GetInstance()
    {
        if (instance == null)
        {
            Debug.LogError("TacticsClient is uninitialized");
            return null;
        }
        return instance;
    }

    // Use this for initialization
    void Awake()
    {
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }

        currentPosition = new Vector2(xPosition, xPosition);
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);

        clientNet.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, "Grognak");
        clientNet.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, -1, 1);
        clientNet.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, true);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            clientNet.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, true);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            clientNet.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, "Grognak");
            clientNet.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, -1, 1);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (movesRemaining > 0)
            {
                Vector2 up = new Vector2(xPosition, (yPosition + 1));
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != up)
                    {
                        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, xPosition, (yPosition + 1));
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");                        
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);

        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            if (movesRemaining > 0)
            {
                Vector2 left = new Vector2((xPosition -1), yPosition);
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != left)
                    {
                        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, (xPosition - 1), yPosition);
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");                        
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            if (movesRemaining > 0)
            {
                Vector2 down = new Vector2(xPosition, (yPosition - 1));
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != down)
                    {
                        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, xPosition, (yPosition - 1));
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            if (movesRemaining > 0)
            {
                Vector2 right = new Vector2((xPosition + 1), yPosition);
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != right)
                    {
                        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, (xPosition + 1), yPosition);
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            if (movesRemaining > 0)
            {
                Vector2 up = new Vector2(xPosition, (yPosition + 1));
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != up)
                    {
                        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, xPosition, (yPosition + 1));
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);

        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (movesRemaining > 0)
            {
                Vector2 left = new Vector2((xPosition - 1), yPosition);
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != left)
                    {
                        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, (xPosition - 1), yPosition);
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            if (movesRemaining > 0)
            {
                Vector2 down = new Vector2(xPosition, (yPosition - 1));
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != down)
                    {
                        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, xPosition, (yPosition - 1));
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (movesRemaining > 0)
            {
                Vector2 right = new Vector2((xPosition + 1), yPosition);
                foreach (Vector2 i in blockedSpaces)
                {
                    if (i != right)
                    {
                        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, (xPosition + 1), yPosition);
                    }
                    else
                    {
                        Debug.Log("Blocked Space!");
                    }
                }
            }
            else
                clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

    }

    public void UpdateState(int x, int y, string player)
    {
        // Update the visuals for the game
    }

    public void RPCTest(int aInt)
    {
        Debug.Log("RPC Test has been called with " + aInt);
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    // Networking callbacks
    // These are all the callbacks from the ClientNetwork
    void OnNetStatusNone()
    {
        Debug.Log("OnNetStatusNone called");
    }
    void OnNetStatusInitiatedConnect()
    {
        Debug.Log("OnNetStatusInitiatedConnect called");
    }
    void OnNetStatusReceivedInitiation()
    {
        Debug.Log("OnNetStatusReceivedInitiation called");
    }
    void OnNetStatusRespondedAwaitingApproval()
    {
        Debug.Log("OnNetStatusRespondedAwaitingApproval called");
    }
    void OnNetStatusRespondedConnect()
    {
        Debug.Log("OnNetStatusRespondedConnect called");
    }
    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);
    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }
    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");

        loginInProcess = false;

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }
    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");

        // Tell the server we are ready
        myPlayer = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        myPlayer.GetComponent<NetworkSync>().AddToArea(1);
    }

    // RPC Called by the server once it has finished sending all area initization data for a new area
    public void AreaInitialized()
    {
        Debug.Log("AreaInitialized called");
    }

    void OnDestroy()
    {
        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    //Client to Server RPCs

    //RequestMove(int x, int y)
    //RequestAttack(int x, int y) adjacent or within 6 spaces if wizard
    //SendChat(string message)
    //SendTeamChat(string message)
    //PassTurn()

    
    //Tactics RPC Functions

    public void SetPlayerID (int ID)
    {
        thisPlayerID = ID;
    }

    public void SetTeam(int team)
    {
        teamId = team;
    }

    public void NewPlayerConnected(int playerID, int team)
    {
        Debug.Log("A new player has joined. ID: " + playerID + " Team: " + team);
    }

    public void PlayerNameChanged(int playerID, string name)
    {
        Debug.Log("A player has changed their name. ID: " + playerID + " New Name: " + name);
    }

    public void PlayerClassChanged(int playerID, int type)
    {
        Debug.Log("A player has changed their Class. ID: " + playerID + " Class: " + type);
    }

    public void PlayerIsReady(int playerID, bool isReady)
    {
        Debug.Log("A player is ready. ID: " + playerID);
    }

    public void GameStart(int time)
    {
        Debug.Log("The game will begin in " + time + " seconds.");
    }

    public void SetMapSize(int x, int y)
    {
        mapX = x;
        mapY = y;
    }

    public void SetBlockedSpace(int x, int y)
    {
        Vector2 V = new Vector2(x, y);
        blockedSpaces.Add(V);
    }

    public void SetPlayerPosition(int playerID, int x, int y)
    {
        if(playerID == thisPlayerID)
        {
            xPosition = x;
            yPosition = y;
            currentPosition = new Vector2(x, y);
        }
        else
            Debug.Log("A player has moved to a new space. ID: " + playerID + " Space: " + x + " , " + y);
    }

    public void AttackMade(int x, int y)
    {
        Debug.Log("A player has attacked space: " + x + " , " + y);
    }

    public void StartTurn(int playerID)
    {
        Debug.Log("A player has started their turn. ID: " + playerID);
    }

    public void DisplayChatMessage(string message)
    {
        Debug.Log("Message: " + message);
    }

    public void UpdateHealth(int playerID, int newHealth)
    {
        Debug.Log("A player's health has changed. ID: " + playerID + " New Health: " + newHealth);
    }




}


