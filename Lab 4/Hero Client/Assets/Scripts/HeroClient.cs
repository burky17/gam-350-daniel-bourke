﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HeroClient : MonoBehaviour
{
    
    [SerializeField]public long clientID;
    public int networkID;
    public string username;
    public bool canMakeHero;    
    public Text statusText;

    public ClientNetwork clientNet;

    public GameObject loginScreen;

    //client instance
    public static HeroClient instance = null;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public float timeToSend = 1.0f;
    public Hero currentHero;

    public class Hero
    {
        public string heroName;
        public float heroSpeed;
        public float heroHealth;
        public float heroAttack;
        public long heroNum;
    }

    void Awake()
    {
        //singleton setup
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;

        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }

       
        canMakeHero = false;
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    //Update moves the playerCube if they are an owned object
    void Update()
    {
        if(canMakeHero)
        {
            statusText.text = "Select a Hero";
        }
        if (!canMakeHero && currentHero != null)
        {
            statusText.text = currentHero.heroName + " sent, please wait";
        }
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        //clientNet.AddToArea(1);
        canMakeHero = true;
    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");        
    }

    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");
        loginInProcess = false;        
    }

    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");
    }

    void OnDestroy()
    {        
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    public void UpdateClientID(long aClientID)
    {
        clientID = aClientID;
        Debug.Log("clientID Updated!");        
    }

    public void UpdateHeroStatus(bool aBool)
    {
        if(aBool)
        {            
            Debug.Log("Server Created Hero");
        }
        if (!aBool)
        {
            canMakeHero = true;
            Debug.Log("Hero");
        }
    }

    public void MakeTank()
    {
        if (canMakeHero)
        {
            Hero newHero = new Hero();
            newHero.heroName = "Tank";
            newHero.heroSpeed = 0.01f;
            newHero.heroHealth = 5.0f;
            newHero.heroAttack = 2.0f;
            newHero.heroNum = clientID;
            currentHero = newHero;
            clientNet.CallRPC("CreateHero", UCNetwork.MessageReceiver.ServerOnly, -1, newHero.heroName, newHero.heroSpeed, newHero.heroHealth, newHero.heroAttack, newHero.heroNum);
            canMakeHero = false;
            Debug.Log("Sent Hero Creation");
        }        
    }

    public void MakeLancer()
    {
        if (canMakeHero)
        {
            Hero newHero = new Hero();
            newHero.heroName = "Lancer";
            newHero.heroSpeed = 0.04f;
            newHero.heroHealth = 1.0f;
            newHero.heroAttack = 5.0f;
            newHero.heroNum = clientID;
            currentHero = newHero;
            clientNet.CallRPC("CreateHero", UCNetwork.MessageReceiver.ServerOnly, -1, newHero.heroName, newHero.heroSpeed, newHero.heroHealth, newHero.heroAttack, newHero.heroNum);
            canMakeHero = false;
            Debug.Log("Sent Hero Creation");
        }
    }

    public void MakeRunner()
    {
        if (canMakeHero)
        {
            Hero newHero = new Hero();
            newHero.heroName = "Runner";
            newHero.heroSpeed = 0.07f;
            newHero.heroHealth = 1.0f;
            newHero.heroAttack = 1.0f;
            newHero.heroNum = clientID;
            currentHero = newHero;
            clientNet.CallRPC("CreateHero", UCNetwork.MessageReceiver.ServerOnly, -1, newHero.heroName, newHero.heroSpeed, newHero.heroHealth, newHero.heroAttack, newHero.heroNum);
            canMakeHero = false;
            Debug.Log("Sent Hero Creation");
        }
    }

    
}
