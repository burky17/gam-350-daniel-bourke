﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject bulletObject;
    public Rigidbody bulletBody;

    // Update is called once per frame
    void Update()
    {        
        Vector3 movement = new Vector3(0, 0.5f, 0);
        transform.position += movement;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != "Player")
        {
            Destroy(bulletObject);
        }
    }
}
