﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroScript : MonoBehaviour
{
    public GameObject heroObject;

    
    public string heroName;
    public float heroSpeed;
    public float heroHealth;
    public float heroAttack;
    public long heroNum;
    

    WaveManager waveManager;

    EvilClient evilClient;

    //Sets references to singletons 
    void Start()
    {
        waveManager = WaveManager.instance;
        evilClient = EvilClient.instance;
    }

    // Update is called once per frame
    void Update()
    {
        //if the wave is active, will move the hero at a set pace toawrds the tower
        if (waveManager.waveActive)
        {
            Vector3 movement = new Vector3(0, -heroSpeed, 0);
            transform.position += movement;
        }

        //destroys the hero when health is gone
        //if (heroHealth <= 0)
        //{
        //    Destroy(heroObject);
        //}
    }

    //upon taking lethal damage, the hero will remove themselves from the activeHeroes Dictionary
    // and then destroy itself, so the WaveManager knows
    public void TakeDamage(float aDamage)
    {
        heroHealth -= aDamage;
        if (heroHealth <= 0)
        {
            evilClient.HeroDeath(heroNum);
            waveManager.activeHeroes.Remove(heroNum);
            Debug.Log("Hero " + heroNum + " removed from dictionary");
            Destroy(heroObject);
            Destroy(this);
        }
    }

    //colliding with a bullet will damage the hero based on player attack
    //colliding with the Tower will damage the player health
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bullet")
        {
            TakeDamage(1);
        }

        if (collision.transform.tag == "Tower")
        {
            evilClient.playerHealth -= heroAttack;
            TakeDamage(1);
        }
    }
}
