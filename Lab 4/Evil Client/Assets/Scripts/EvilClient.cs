﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EvilClient : MonoBehaviour
{
    public GameObject bulletPrefab;    
    public long clientID = 6969;
    public int networkID;
    public string username = "Sauron";
    public float playerHealth = 10;
    public float playerAttack = 2;  
    public float playerSpeed = 5;
    public bool isDarkLord = true;
    public bool needsAssignment = false;
    public Text playerHealthText;

    public WaveManager waveManager;

    public GameObject playerCube;

    public Renderer playerRenderer;

    public NetworkSync playerNetSync;

    public ClientNetwork clientNet;

    public GameObject loginScreen;

    //client instance
    public static EvilClient instance = null;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public float timeToSend = 1.0f;


    void Awake()
    {
        //singleton setup
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;

        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }

        if (waveManager == null)
        {
            waveManager = WaveManager.instance;
        }
        playerHealth = 10;
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 6969);
    }

    //Update moves the playerCube if they are an owned object
    void Update()
    {
        if (playerNetSync != null)
        {
            if (networkID != playerNetSync.networkId)
            {
                networkID = playerNetSync.networkId;
                //clientNet.CallRPC("UpdateNetID", UCNetwork.MessageReceiver.ServerOnly, -1, clientID);

            }
        }

        playerHealthText.text = "Health: " + playerHealth;
       

        //has the server assign the Dark Lord title to the player on the EvilClient 
        if (needsAssignment)
        {
            clientNet.CallRPC("AssignDarkLord", UCNetwork.MessageReceiver.ServerOnly, -1, clientID);
            needsAssignment = false;
        }

        //fires the bullet that kills heroes
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FireProjectile();
        }

        //starts the wave early 
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!waveManager.waveActive)
            {
                waveManager.StartWave();
            }
        }

        //starts the wave early 
        if (Input.GetKeyDown(KeyCode.Q))
        {
            waveManager.CreateRedShirt();
        }

        //dies if health is gone
        if (playerHealth <= 0)
        {
            OnDestroy();
        }
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);

    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");

        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
    }

    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");

        loginInProcess = false;

        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
    }

    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");

        // Tell the server we are ready
        playerCube = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        playerCube.GetComponent<NetworkSync>().AddToArea(1);
        playerNetSync = playerCube.GetComponent<NetworkSync>();
        //set starting color
        playerRenderer = playerCube.GetComponent<Renderer>();
        playerRenderer.material.color = Color.blue;

        needsAssignment = true;


    }

    //on destroy function
    void OnDestroy()
    {
        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }
    
    public void ServerNeedsHealth()
    {
        clientNet.CallRPC("UpdateDarkLordHealth", UCNetwork.MessageReceiver.ServerOnly, -1, playerHealth);
        Debug.Log("Told server that health is: " + playerHealth);
    }

    //Updates the clientID to the one received from the server
    public void UpdateClientID(long aClientID)
    {
        clientID = aClientID;
        Debug.Log("clientID Updated!");
        clientNet.CallRPC("AssignDarkLord", UCNetwork.MessageReceiver.ServerOnly, -1, clientID);
    }

    //Updates the number of queued heroes to the server
    public void HeroDeath(long aHeroNum)
    {
        clientNet.CallRPC("HeroDeath", UCNetwork.MessageReceiver.ServerOnly, -1, aHeroNum);
    }
    
    public void CreateLocalHero(string aHeroName, float aHeroSpeed, float aHeroHealth, float aHeroAttack, long aHeroNum)
    {
        waveManager.CreateWaveHero(aHeroName, aHeroSpeed, aHeroHealth, aHeroAttack, aHeroNum);
        Debug.Log("Sent off to wave manager");
    }

    //fires the projectile
    public void FireProjectile()
    {
       GameObject bullet = Instantiate(bulletPrefab, playerCube.transform);        
    }

    //colliding with a Hero will damage the player
    private void OnCollisionEnter(Collision collision)
    {        

        if (collision.transform.tag == "Hero")
        {
            playerHealth -= collision.gameObject.GetComponent<HeroScript>().heroAttack;
        }        

    }

}
