﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public EvilClient evilClient;
    public static WaveManager instance;
    public GameObject heroPrefab;

    public List<Hero> waitingHeroes = new List<Hero>();
    public List<Hero> readyHeroes = new List<Hero>();
    public Dictionary<long, GameObject> activeHeroes = new Dictionary<long, GameObject>();


    public GameObject spawnPosition;
    public bool waveReady = false;
    public float waveTimer = 0;
    public bool waveActive = false;
    public bool waveOver = false;
    public long redShirtCatalog = 0;

    public class Hero
    {
        public string heroName;
        public float heroSpeed;
        public float heroHealth;
        public float heroAttack;
        public long heroNum;
    }

    void Awake()
    {
        //singleton setup
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;



        evilClient = EvilClient.instance;

    }

    // Update is called once per frame
    void Update()
    {
        //waveTimer -= Time.deltaTime;       


        //if (waveTimer <= 0 && waveReady && !waveActive && waveOver)
        //{
        //    StartWave();
        //}

        if (waitingHeroes.Count > 5 && !waveActive)
        {
            StartWave();
        }
      


        //checks to see if the activeHeroes are all dead and ends the wave if so
        if (activeHeroes.Count <= 0 && waveActive)
        {
            Debug.Log("Wave is over");

            waveOver = true;
        }

        //resets the wave to the prep phase
        if (waveOver)
        {
            waveActive = false;
            waveReady = false;
            //waveTimer = 60f;
            waveOver = false;
        }      
       
    }

    //can be called by the player or when the timer runs out, whichever comes first
    public void StartWave()
    {
        PrepareWave();

        if (waveReady)
        {
            SpawnWave();
        }
    }    

    //Preps the wave by transferring the heroes from the waitingHeroes List to the
    // readyHeroes List, and then sets waveReady to true
    //If no heroes were made during the wave, it creates a simple RedShirt hero
    public void PrepareWave()
    {
        if (waitingHeroes.Count > 0)
        {
            int waveCount = waitingHeroes.Count;

            foreach (Hero h in waitingHeroes)
            {
                readyHeroes.Add(h);
            }

            waitingHeroes.Clear();

            Debug.Log("PrepareWave() done, added" + waveCount + " heroes");

            waveReady = true;
        }
    }

    //Spawns the heroes into the world based on the readyHeroes List, copies the data over
    // to the spawned hero, adds it to the activeHeroes Disctionary and assigns the numHero int
    // to the hero as both an ID and the Key for the Dictionary for later reference if it gets destroyed,
    // so the Hero can self-report it's own death and remove itself from the Dictionary
    public void SpawnWave()
    {
        int numHeroes = 0;

        foreach (Hero h in readyHeroes)
        {
            numHeroes++;
            GameObject newHero = Instantiate(heroPrefab, spawnPosition.transform);
            PassHeroData(h, newHero.GetComponent<HeroScript>());
            newHero.transform.position += new Vector3(numHeroes * 2, 0, 0);
            activeHeroes.Add(h.heroNum, newHero);
            Debug.Log(h.heroName + " Hero ID: " + h.heroNum + " added to ActivePlayers");
        }

        waveReady = false;
        readyHeroes.Clear();
        Debug.Log("SpawnWave() is done");
        numHeroes = 0;
        waveActive = true;
    }

    //Creates a Hero from the arguments passed in and loads it into the waitingHeroes List
    public void CreateWaveHero(string aHeroName, float aHeroSpeed, float aHeroHealth, float aHeroAttack, long aHeroNum)
    {
        Hero newHero = new Hero();
        newHero.heroName = aHeroName;
        newHero.heroSpeed = aHeroSpeed;
        newHero.heroHealth = aHeroHealth;
        newHero.heroAttack = aHeroAttack;
        newHero.heroNum = aHeroNum;
        waitingHeroes.Add(newHero);
        Debug.Log("Server called Create Hero Successfully from HeroClientID: " + aHeroNum);
    }

    //Creates a default soldier
    public void CreateRedShirt()
    {
        redShirtCatalog++;
        Hero newHero = new Hero();
        newHero.heroName = "Cannon Fodder";
        newHero.heroSpeed = 0.03f;
        newHero.heroHealth = 1;
        newHero.heroAttack = 1;
        newHero.heroNum = redShirtCatalog;
        waitingHeroes.Add(newHero);
        Debug.Log("RedShirt Created");
    }

    //copies the hero data over from the hero class to the spawned hero
    public void PassHeroData(Hero heroData, HeroScript spawnHero)
    {
        spawnHero.heroName = heroData.heroName;
        spawnHero.heroSpeed = heroData.heroSpeed;
        spawnHero.heroHealth = heroData.heroHealth;
        spawnHero.heroAttack = heroData.heroAttack;        
        spawnHero.heroNum = heroData.heroNum;
    }

    
}
