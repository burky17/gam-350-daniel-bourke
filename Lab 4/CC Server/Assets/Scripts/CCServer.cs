﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCServer : MonoBehaviour
{
    public static CCServer instance;

    public ServerNetwork serverNet;

    public int portNumber = 603;

    public bool gameRunning = false;

    public bool waveActive = false;

    public List<Hero> waitingHeroes = new List<Hero>();
    public Dictionary<long, Hero> activeHeroes = new Dictionary<long, Hero>();
    public int heroesQueued;
    public int slotsOpen;

    

    public class Hero
    {
        public string heroName;
        public float heroSpeed;
        public float heroHealth;
        public float heroAttack;
        public long heroNum;
    }

    // Stores a player
    class Player
    {
        public long clientId;
        public int networkID;
        public string playerName;
        public int health;
        public int attack;
        public int speed;
        public bool isDarkLord;
        public bool isConnected;
        public ServerNetwork.NetworkObject netObj;
    }

    //list of the heroes put in the wave
    //List<Player> heroWave = new List<Player>();

    //list of all the players in the game
    [SerializeField]
    List<Player> allPlayers = new List<Player>();


    //List of Networked Objects
    Dictionary<int, ServerNetwork.NetworkObject> playerObjects;

    //the Dark Lord client reference is here
    [SerializeField]
    Player darkLord;

    

    // Use this for initialization
    void Awake()
    {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        
    }


    void Update()
    {
        //Checks each frame if the DarkLord is connected, then starts the game once they are.        
        if (!gameRunning)
        {
            if (darkLord != null)
            {                
               if (darkLord.isConnected)
               {
                    serverNet.CallRPC("ServerNeedsHealth", darkLord.clientId, -1);
                    gameRunning = true;                  
               }                
            }
        }

        
        if (gameRunning)
        {             
            if (waitingHeroes.Count > 0)
            {
                foreach (Hero h in waitingHeroes)
                {
                    serverNet.CallRPC("CreateLocalHero", darkLord.clientId, -1, h.heroName, h.heroSpeed, h.heroHealth, h.heroAttack, h.heroNum);
                    activeHeroes.Add(h.heroNum, h);                   

                }
                waitingHeroes.Clear();
            }

        }
    }

    //Client asks to join the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username + " at clientID: " + data.id);
        

        //Create the player on the server and add it to the list
        {
            Player newPlayer = new Player();
            newPlayer.clientId = data.id;
            newPlayer.playerName = data.username;
            newPlayer.isDarkLord = false;           
            newPlayer.isConnected = false;            
            allPlayers.Add(newPlayer);
            serverNet.ConnectionApproved(data.id);
            
        }

        

    }

    public void AssignDarkLord(long aClientID)
    {
        //Function only to be called from the EvilClient on initialization

        foreach (Player p in allPlayers)
        {
            if (p.clientId == aClientID)
            {
                p.isDarkLord = true;
                darkLord = p;
            }
        }
    }
    
    //Sent from EvilClient to update Health
    void UpdateDarkLordHealth(int aHealth)
    {
        darkLord.health = aHealth;
    }

    //Sent from EvilClient to update 
    public void HeroDeath(long aHeroNum)
    {
        Debug.Log("A hero associated with CLient ID: " + aHeroNum + " has died");
        serverNet.CallRPC("UpdateHeroStatus", aHeroNum, -1, false);
        activeHeroes.Remove(aHeroNum);
    }

    void OnClientConnected(long aClientId)
    {
        serverNet.CallRPC("UpdateClientID", aClientId, -1, aClientId);

        // Set the isConnected to true on the player
        foreach (Player p in allPlayers)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = true;
            }
        }

        

    }

    void OnClientDisconnected(long aClientId)
    {
        // Set the isConnected to false on the player
        //if the player was It, set the currentlyIt to null
        foreach (Player p in allPlayers)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = false;
                
                if (p.clientId == darkLord.clientId)
                {
                    darkLord = null;
                }
            }
        }
    }

    //Takes a clientID and a networkID from a client and links the owned
    //networked object to the player class for future reference
    //void UpdateNetID(int aNetworkID, long aClientID)
    //{
    //    foreach (int key in serverNet.networkObjects.Keys)
    //    {
    //        if (aNetworkID == key)
    //        {
    //            if (aClientID == darkLord.clientId)
    //            {
    //                darkLord.netObj = serverNet.networkObjects[key];
    //            }
    //        }

    //    }
    //}

    public void CreateHero(string aHeroName, float aHeroSpeed, float aHeroHealth, float aHeroAttack, long aHeroNum)
    {
        Debug.Log("Create Hero Debug Before Assignment");
        Hero newHero = new Hero();
        newHero.heroName = aHeroName;
        newHero.heroSpeed = aHeroSpeed;
        newHero.heroHealth = aHeroHealth;
        newHero.heroAttack = aHeroAttack;
        newHero.heroNum = aHeroNum;
        waitingHeroes.Add(newHero);
        serverNet.CallRPC("UpdateHeroStatus", aHeroNum, -1, true);
        Debug.Log("Create Hero Debug After Assignment");

    }

}
