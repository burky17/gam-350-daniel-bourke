﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TagClient : MonoBehaviour
{

    public float playerSpeed = 5;
    public bool isIt;
    public bool isTaggable;
    public float boostTimer;
    public long clientID;
    public int networkID;
    public string username;

    public GameObject playerCube;
    public Renderer playerRenderer;

    public NetworkSync playerNetSync;

    public ClientNetwork clientNet;

    public GameObject loginScreen;

    //client instance
    public static TagClient instance = null;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public float timeToSend = 1.0f;


    void Awake()
    {
        //singleton setup
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;

        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }              

        //sets starting states        
        isIt = false;
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    //Update moves the playerCube if they are an owned object
    void Update()
    {
        if(playerNetSync != null)
        {
            if (networkID != playerNetSync.networkId)
            {
                networkID = playerNetSync.networkId;
                clientNet.CallRPC("UpdateNetID", UCNetwork.MessageReceiver.ServerOnly, -1, networkID, clientID);
            }

            //if (GetComponent<NetworkSync>().owned)
            //{
            //    Vector3 playerMove = new Vector3(Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime, Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime, 0);
            //    transform.position += playerMove;
            //}

            if (!isTaggable)
            {
                if (boostTimer > 0)
                {
                    boostTimer -= Time.deltaTime;
                }

                if (boostTimer < 0)
                {
                    YouAreTaggable();
                }
            }
        }
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);
    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");

        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
    }

    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");

        loginInProcess = false;

        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
    }

    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");

        // Tell the server we are ready
        playerCube = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        playerCube.GetComponent<NetworkSync>().AddToArea(1);
        playerNetSync = playerCube.GetComponent<NetworkSync>();
        //set starting color
        playerRenderer = playerCube.GetComponent<Renderer>();
        playerRenderer.material.color = Color.blue;

    }

    void OnDestroy()
    {
        if (playerCube)
        {
            clientNet.Destroy(playerCube.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    //Handles local logic for being marked "It"
    public void YouAreIt()
    {
        playerRenderer.material.color = Color.red;        
        isIt = true;
        isTaggable = false;
        playerSpeed = 7;
        Debug.Log("You Are It!");
    }

    //Handles local logic for being marked "It"
    public void YouWereIt()
    {
        playerRenderer.material.color = Color.yellow;
        isIt = false;
        isTaggable = false;
        boostTimer = 5;
        playerSpeed = 10;
        Debug.Log("Immune for 5 seconds.");
        
    }

    //Makes the object taggable again
    public void YouAreTaggable()
    {
        playerRenderer.material.color = Color.blue;      
        isTaggable = true;
        boostTimer = 0;
        playerSpeed = 5;
        Debug.Log("Immunity over.");

    }

    //Updates the clientID to the one received from the server
    public void UpdateClientID(int aClientID)
    {
        clientID = aClientID;
    }
}
