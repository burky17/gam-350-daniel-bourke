﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagServer : MonoBehaviour
{
    public static TagServer instance;

    public ServerNetwork serverNet;

    public int portNumber = 603;

    public bool gameRunning = false;

    // Stores a player
    class Player
    {
        public long clientId;
        public int networkID;
        public string playerName;
        public bool isIt;
        public bool isTaggable;
        public float tagbackTimer;
        public bool isConnected;
        public ServerNetwork.NetworkObject netObj;
    }

    //list of players in the game
    List<Player> tagPlayers = new List<Player>();

    //List of Networked Objects
    //Dictionary<int, ServerNetwork.NetworkObject> playerObjects;

    //the currentlyIt player stored here
    Player currentlyIt;

    

    // Use this for initialization
    void Awake()
    {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        
    }


    void Update()
    {
        //Checks each frame for at least one connected player, then assigns them to "currentlyIt"
        //if they are connected and begins the game
        if (!gameRunning)
        {
            if (tagPlayers.Count >= 2)
            {
                foreach (Player p in tagPlayers)
                {
                    if (p.isConnected)
                    {
                        p.isIt = true;
                        p.isTaggable = false;
                        currentlyIt = p;
                        serverNet.CallRPC("YouAreIt", currentlyIt.clientId, -1);
                        gameRunning = true;
                        break;
                    }
                }
            }
        }

        
        if (gameRunning)
        {
            //Handles the tagbackTimer
            foreach (Player p in tagPlayers)
            {
                if (!p.isTaggable)
                {
                    if (p.tagbackTimer > 0)
                    {
                        p.tagbackTimer -= Time.deltaTime;
                    }

                    if (p.tagbackTimer < 0 && !p.isIt)
                    {
                        p.isTaggable = true;
                        p.tagbackTimer = 0;
                    }
                }
            }

            //if no one is It but players are in the game, will assign one to be It
            if (currentlyIt == null && tagPlayers.Count > 0)
            {
                foreach (Player p in tagPlayers)
                {
                    if (p.isConnected)
                    {
                        p.isIt = true;
                        p.isTaggable = false;
                        currentlyIt = p;
                        serverNet.CallRPC("YouAreIt", currentlyIt.clientId, -1);
                        gameRunning = true;
                        break;
                    }
                }
            }

            //Stope the game loop if no players are in the game
            if (currentlyIt == null && tagPlayers.Count == 0)
            {
                gameRunning = false;
            }

            //Gets the NetworkObject that is IT
            ServerNetwork.NetworkObject IT = currentlyIt.netObj;

            //Iterates and finds if any player is overlapping the player tha tis IT
            //If they are, and the other player can be tagged, swaps the 
            foreach (Player p in tagPlayers)
            {
                if (p.netObj.position == IT.position && p.isTaggable)
                {
                    currentlyIt.isIt = false;
                    currentlyIt.isTaggable = false;
                    currentlyIt.tagbackTimer = 5;
                    serverNet.CallRPC("YouWereIt", currentlyIt.clientId, -1);

                    p.isIt = true;
                    p.isTaggable = false;
                    p.tagbackTimer = 0;
                    serverNet.CallRPC("YouAreIt", p.clientId, -1);

                    currentlyIt = p;
                
                }
            }

        }
    }

    //Client asks to join the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        //Create the player on the server and add it to the list
        {
            Player newPlayer = new Player();
            newPlayer.clientId = data.id;
            newPlayer.playerName = data.username;
            newPlayer.isIt = false;
            newPlayer.isTaggable = true;
            newPlayer.tagbackTimer = 0;            
            newPlayer.isConnected = false;            
            tagPlayers.Add(newPlayer);

            serverNet.ConnectionApproved(data.id);
            serverNet.CallRPC("UpdateClientID", newPlayer.clientId, -1, newPlayer.clientId);
        }
        
    }

    void OnClientConnected(long aClientId)
    {
        // Set the isConnected to true on the player
        foreach (Player p in tagPlayers)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = true;
            }
        }        
    }

    void OnClientDisconnected(long aClientId)
    {
        // Set the isConnected to false on the player
        //if the player was It, set the currentlyIt to null
        foreach (Player p in tagPlayers)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = false;
                
                if (p.clientId == currentlyIt.clientId)
                {
                    currentlyIt = null;
                }
            }
        }
    }

    //Takes a clientID and a networkID from a client and links the owned
    //networked object to the player class for future reference
    void UpdateNetID (int aNetworkID, long aClientID)
    {
         foreach (int key in serverNet.networkObjects.Keys)
        { 
            if (aNetworkID == key)
            {
                foreach (Player p in tagPlayers)
                {
                    if (aClientID == p.clientId)
                    {
                        p.netObj = serverNet.networkObjects[key];
                    }
                }
            }

        }
    }
    
}
