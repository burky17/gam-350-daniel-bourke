﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    float speed = 5;

    public bool isPaused = false;

    enum TagState
    {
        NONE,
        IT,
        NOT_IT,
        END
    }
    TagState myTagState;

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned && !isPaused)
        {
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
            transform.position += movement;
        }
    }

    public void OnGainOwnership()
    {

    }

    public void PauseMove()
    {
        isPaused = true;
    }

    public void UnpauseMove()
    {
        isPaused = false;
    }
}
